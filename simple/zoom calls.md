zoom calls

Quarantine Coffee Hour
Every Tuesday, 9-10am, for the duration of stay-at-home
Zoom Link | Meeting ID: 872 729 658 (edited) 
https://mhcc.zoom.us/j/872729658?status=success

The Red Caucus of DSA is hosting a reading and discussion of the Communist Manifesto in two sessions:
Sunday, May 3rd at 2:00 - 3:30 PM
Saturday, May 9th at 3:00 - 4:30 PM (thus avoiding Mothers Day)

:rose: New Member Happy Hour :rose:
Saturday, May 2, 5:00 - 7:00 pm
Zoom details in thread
https://mhcc.zoom.us/j/91839479171?pwd=TC9kMWlucXdlQkl1eUJmeFNxN3YwZz09

:rosefists::wave::skin-tone-3: New and Non-Member Orientation:wave::skin-tone-3::rosefists:
Wednesday, April 29, 7:00pm
https://mhcc.zoom.us/j/96602686867?pwd=UlZCRzMra3Z6OWFWUi9xMVduc240QT09nf