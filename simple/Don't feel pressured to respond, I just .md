---
modified: 2020-12-27T03:21:49-08:00
---

Don't feel pressured to respond, I just needed to say this. I hope your only regret is for how Luke might feel and not regret for being with me. That was the most intimate I've felt with anyone before and I will always cherish it regardless of where we go from here. 


#lovelife