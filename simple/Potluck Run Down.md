Potluck Run Down: 

Please confirm your attendance by messaging 
PDX/Vanc area - "Leah aka Maow Maow" directly at @maowmaow:nopasaran.gq
Sea/Tac area - "Koff " directly at @koff:nopasaran.gq

SITE: As per the site hosts: 
IF YOU DO NOT CONFIRM, THERE IS A GOOD POSSIBILITY YOU MAY NOT BE ALLOWED TO ENTER THE COMPOUND. 

WEAPONS: There will be NO LIVE FIREARMS, please keep them in your vehicle! 
Props like airsoft may be used, however the airsoft pellets are banned from this location due to environmental impact.

ATTIRE: Dress warmly, bring blankets and extra layers, it gets really cold and moist at night. Please bring whatever gear to wear you would normally be playing Minecraft in.

FLASHLIGHTS: It gets really dark at night, please bring flashlights. These can also be used as props for minecraft. (Details at potluck)

LAVATORY: Bathroom facilities are severely limited, be prepared to dig a hole in the woods outside the back gate. Please limit urination to fence areas on the north side. 

ARRIVAL TIME: Is after 3pm, please do not show up early unless you have authorization from BRPH directly. (Exceptions can be made for long distance travelers.)

FOOD: This is a potluck too, please bring something to share with others for Saturday Evening or Sunday Morning.

BRING WATER: While water is available, the pipes are old, and slightly rusty, it's potable just not clear. Also bring containers for drinking.

And don't forget personal snacks too. 

ITINERARY:
tentative schedule for evening.Potluck  11/7 -11/8
*all scenarios and times subject to change depending on how long each scenario actually takes to complete
4-5 pm arrivals
5pm- introductions
5:30pm - 6:30pm Dinner and Intro to stealth and recon conversation/class, Review of MOUT, and Light, litter, and noise dicipline.   
6:30pm - 7pm  Go over details of scenario (first one is recon only)    
7pm-9ishpm  Recon scenario
9pm-9:30 Details of defense of compound scenario
9:30-11pm Defense scenario 
11pm-11:30am Details of "VIP" scenario. (extraction and protection of VIP)
11:30pm-1:30am  VIP scenario
1:30am-2am Review of evening and discussion on moving forward 


Morning will be heavily dependant on how late night scenarios run but ideally; (allowing for 6ish hours of sleep for night crew)
9am-10am Warm-up, stretch, light workout, cardio (maybe a run, but I'm leading it, it will be a very slow mile) cool down. (If Kilo is coming and wants to run this time...I'm all about morning Field Fit)
10:30am everyone awake, caffinated,  fed, packed up and ready for the day.  
11am meet for Assisting injured comrade with active contact scenario details.
11:15am - until completed  ICAC scenario
Things that might change;
There is an option to to head to a nearby public range either before or after the scenario.  I would also like to include a review of basic combat medic at some point in the morning. If no range time will happen, then there will more movement drills at the 11am start time, followed by a lunch, Medic stuff, then scenario.    Each scenario might run more than once to give people the chance to be on each side of the scenario.  All scenarios will take place inside buildings. 