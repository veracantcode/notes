Ive been cagey abt some stuff in my life for a while except with a few people, but I want to share vulnerability where I can.

Last month I went through an awful breakup.

I'm doing much better now--I've got some really wonderful people in my life which helps a lot-- though this heartbreak feels like it's going to last forever and I'm never going to fully heal. 

In a way I feel like I'm coming off of an addiction and there's less color in the world now. Is that a common experience? 

I know there were many aspects of my relationship that weren't healthy.. I think I lost some of myself trying to hold steady something I just wanted to keep so badly, even though it was sucking out all my sense of self in the process.

But.. I just keep loving the people I've loved. Even if things didn't work out in the capacity of a relationship, or I need to set strong boundaries to protect myself in the future, I just keep caring about those I've loved before. 

So anyway, I'm ok, and I'll be ok, but this is hard and is just gonna be hard. 

To everyone who cares about me, thank you. I love you ❤