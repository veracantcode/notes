Here's the plan:
I'm hoping we can all meet at my place at 1pm and caravan out to Barton.

My address is 3524 SE Madison, and there's a parking lot across the street from my house where I figure we could meet and get situated before heading out together. I think we have ample floats, but if you have a tube, bring it. Bring anything you want to snack on or drink while on the way. I have a couple dry bags, but if you have your own, I'd recommend bringing it.